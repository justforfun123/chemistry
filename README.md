This Repo contains an interactive Periodic Table GUI written by Derek and David.

Periodic Table GUI features
-When an element button is pressed, the element name and facts about the element appear in a new window

Periodic Table extra tasks
-The conversion window converts a chemical equation
-The search window allows users to search for an element by name or symbol.
-More features will be added (naming ionic compounds, balancing equations,... basically Chem 200 tasks at EHS)

To run the program (sorry for the mess, will clean up when migrate to intellij)
-Download zip and extract
-Navigate to (in terminal) /...path-to-repo.../chemistry/PeriodicTable
-Run program in 2 ways: -java PTGUI
                        -./run.sh

Note: The code in the Repo is sometimes (rarely) uploaded when the program still has errors and can't be run.  If that occurs when trying to run the code, check back again periodically (pun intended) for error corrections.

Last updated: Dec. 28, 2015