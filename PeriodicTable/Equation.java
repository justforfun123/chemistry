/** Chemical Equation
    @author Derek Pham
**/
///*** Warning: This is not perfect
//// Because an equation also needs prefixes in front of each formula
import java.util.ArrayList;

public class Equation{
    private ArrayList<ChemFormula> reactors;
    private ArrayList<ChemFormula> products;
    private boolean balancable;
    private boolean balanced;

    public Equation(ArrayList<ChemFormula> reactors, ArrayList<ChemFormula> products){
	for(ChemFormula formula: reactors)
	    this.reactors.add(formula);
	for(ChemFormula formula: products)
	    this.products.add(formula);
    }

    // Writing in the future
    // Whether the reactor and products side have the same elements
    public boolean isValid(){
	return false;
    }

    // Writing in the future
    // Whether the equation can be balanced or not
    public boolean isBalancable(){
	return false;
    }

    // auto-balance the equation if not balanced ==  false
    public void autoBalance(){
	
    }
}
