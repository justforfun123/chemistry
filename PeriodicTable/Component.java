/** Component interface
    @author Derek Pham
**/

public interface Component{
    public double getMass();
    public String toString();
}
