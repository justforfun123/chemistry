textFile = ''
with open("chemdb") as chem_file:
    for line in chem_file:
        list_component = line.split(":")
        if list_component[5] == "Alkali":
            list_component[5] = "Alkali Metal"
        elif list_component[5] == "Alkaline Earth":
            list_component[5] = "Alkaline Earth Metal"
        elif list_component[5] == "Transition":
            list_component[5] = "Transition Metal"
        index = 2
        if int(list_component[index]) < 72 and int(list_component[index]) > 56:
            list_component[5] = "Lanthanide Series"
        elif int(list_component[index]) < 104 and int(list_component[index]) > 88:
            list_component[5] = "Actinide Series"
        new_line = ":".join(list_component)
        textFile += new_line
print textFile
